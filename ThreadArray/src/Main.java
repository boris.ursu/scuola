import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main extends Thread {

    public static ThreadArray[] threadArrays;

    public static void main(String[] args) throws InterruptedException {

        Scanner sc = new Scanner(System.in);
        Random rand = new Random();

        double temp = System.nanoTime();
        double temp1 = System.nanoTime();
        int n;
        int increment;
        int num;
        int[] a;
        double tempo = 0;

        tempo = (temp1 - temp);

        System.out.print("Inserire la dimensione array: ");
        n = sc.nextInt();
        System.out.print("Numero di volte in cui si vuole dividere l'array: ");
        num = sc.nextInt();

        if(n % num != 0) {

            System.out.println("Array è di lunghezza dispari verrà diminuito finchè si può dividere in parti uguali");

            while (n % num != 0) {
                n = n - 1;
            }

        }

        increment = n / num;
        threadArrays = new ThreadArray[num];
        a = new int[n];

        int from = 0;
        int to = increment;

        for(int i = 0; i < a.length; i++){
            a[i] = rand.nextInt(a.length+1);
        }


        for(int i = 0; i < num; i++){
            ThreadArray ta = new ThreadArray(Arrays.copyOfRange(a, from, to));
            ta.run();
            threadArrays[i] = ta;
            from = to;
            to += increment;
        }

        for (int i = 0; i < threadArrays.length; i++) {
            ThreadArray thread = threadArrays[i];
            thread.join();
        }
        Arrays.sort(a);
        System.out.println(Arrays.toString(mergeSort(threadArrays[0].arr, threadArrays[1].arr, 1)));
        System.out.println("Tempo impiegato : " + tempo + " in ns");
    }

    static int[] mergeSort(int[] arr, int[] arr1, int n) {

        int[] ar = new int[arr.length + arr1.length];

        int i = 0;
        int j = 0;
        int k = 0;

        while (j < arr.length && k < arr1.length) {
            if (arr[j] < arr1[k])
                ar[i++] = arr[j++];
            else
                ar[i++] = arr1[k++];
        }

        while (j < arr.length)
            ar[i++] = arr[j++];
        while (k < arr1.length)
            ar[i++] = arr1[k++];

        if (n != threadArrays.length - 1)
            return mergeSort(ar, threadArrays[n + 1].arr, n + 1);
        else
            return ar;
    }
}


