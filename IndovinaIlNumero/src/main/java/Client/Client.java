package Client;

import java.net.Socket;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Scanner;
import java.net.UnknownHostException;
import java.io.IOException;
import java.util.ArrayList;
import java.io.BufferedInputStream;

public class Client {

    public static Socket socketServer;
    public static DataInputStream in;
    public static DataOutputStream out;
    public static Scanner sc = new Scanner(System.in);

    //metodo che serve per connetere il client con il server
    public Socket connetti(){
        Socket server = null;

        try{
            System.out.println("Connssione in corso con il server");
            server = new Socket("localhost", 6789);
            System.out.println("Connessione stabilita");
            in = new DataInputStream(server.getInputStream());
            out = new DataOutputStream(server.getOutputStream());
        } catch(UnknownHostException e){
            System.err.println("Host sconosciuto");
        } catch (Exception e) {
            System.err.println("Impossibile stabilire una connesione");
            System.exit(1);
        }
        return server;
    }

    //metodo che controlla se l'imput è inserito in modo correto
    private static boolean controlloInserimento(String s){
        ArrayList<Character> al = new ArrayList<>();
        char[] a = s.toCharArray();
        char[] a1 = s.toCharArray();

        for (int i = 0; i < a.length; i++) {
            char c = a[i];
            if (al.contains(c)) {
                return false;
            } else {
                al.add(c);
            }
        }

        for (int i = 0; i < a1.length; i++) {
            char c = a1[i];
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }

    //metodo che controlla la risposta
    public String controlloRisposta(){
        String s = "";

        try {
            s = in.readUTF();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(s.equals("Hai indovinato!")) {
            System.out.println(s);
            System.exit(0);
        }
        return s;
    }

    //Nel metodo main
    public static void main(String[] args) {
        Client cli = new Client();
        socketServer = cli.connetti();

        try {
            in = new DataInputStream(new BufferedInputStream(socketServer.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        while(true) {

            String s = "";
            System.out.print("Prova a indovinare il numero: ");
            s = sc.nextLine();
            if(controlloInserimento(s)){
                try {
                    out.writeUTF(s);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println(cli.controlloRisposta());
            } else {
                System.out.println("Hai sbagliato a inserire!");
            }
        }
    }
}