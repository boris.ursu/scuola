package Server;

import java.net.Socket;
import java.net.ServerSocket;
import java.io.IOException;

public class Server {

    public static Socket socket;
    public static ServerSocket serverSocket;

    //Nel metodo main c'è il tentativo  di connessione tra il server e il client
    public static void main(String[] args) {
        try{
            serverSocket = new ServerSocket(6789); //numero porta
        } catch (IOException e) {
            e.printStackTrace();
        }
        while(true){
            try{
                socket = serverSocket.accept();
            } catch (IOException e) {
                e.printStackTrace();
            }
            CotrolloGioco thread = new CotrolloGioco(socket);
            thread.start();
        }
    }
}