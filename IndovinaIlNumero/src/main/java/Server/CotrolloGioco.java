package Server;

import java.net.Socket;
import java.util.ArrayList;
import java.io.DataInputStream;
import java.util.Random;
import java.io.DataOutputStream;
import java.io.IOException;



public class CotrolloGioco extends Thread {

    public Random rnd = new Random();
    public String numInt = "";
    public Socket socketClient = null;

    private ArrayList<Integer> numeri = new ArrayList<>();

    protected CotrolloGioco(Socket socket){
        socketClient = socket;
        numeri = riempimentoArraylist();
        numInt = convertInput(numeri);
    }

    DataInputStream in;
    DataOutputStream out;
    //Il metodo serve per ricevere la richiesta del Client ritornando la risposta in Stringa
    public String controlloRichiesta(){
        String s = "";

        try{
            in = new DataInputStream(socketClient.getInputStream());
        } catch(Exception e){
            e.printStackTrace();
        }

        try {
            s = in.readUTF();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }

    //Metodo per riepire l'arraylist con numeri casuali
    private ArrayList<Integer> riempimentoArraylist(){
        int num = 0;
        while(numeri.size() < 4){
            num = rnd.nextInt(10);
            if(!numeri.contains(num))
                numeri.add(num);
        }
        return numeri;
    }

    //Prende il numero inserito e lo converte in arraylist
    private ArrayList<Integer> convertInput(String s){
        ArrayList<Integer> al = new ArrayList<>();
        for (int i = 0; i<s.length(); i++){
            al.add(Character.getNumericValue(s.charAt(i)));
        }
        return al;
    }

    //Prende il numero inserito e lo converte in Stringa
    private String convertInput(ArrayList<Integer> al){
        String s = "";
        for (int i = 0; i < al.size(); i++) {
            Integer inte = al.get(i);
            s += inte.toString();
        }
        return s;
    }
    //Un override del metodo run della classe Thread
    @Override
    public void run(){
        int pos;
        int num;
        String s;

        try{
            out = new DataOutputStream(socketClient.getOutputStream());
        }catch(Exception e){
            e.printStackTrace();
        }

        System.out.println(numInt);

        while(true){
            s = controlloRichiesta();
            pos = controlloPosizione(numeri, convertInput(s));
            num = controlloNumero(numeri, convertInput(s));
            if(s.equals(numInt)){

                try {
                    out.writeUTF("Hai indovinato!");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;

            } else {

                try {
                    out.writeUTF("Numeri in posizione correta: " + pos + "\n"+ "Numeri inseriti corretamente ma in posizione sbagliata: " + (num - pos) + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //Metodo che fa una comparazione tra il numero inserito dall'utente e il numero sconosciuto per vedere se i numeri si trovano nella posizione giusta
    private int controlloPosizione(ArrayList<Integer> numInte, ArrayList<Integer> al){
        int num = 0;
        for(int i = 0; i < 4; i++){
            if(numInte.get(i) == al.get(i))
                num++;
        }
        return num;
    }

    ////Metodo che fa una comparazione tra il numero inserito dall'utente e il numero sconosciuto per vedere se i numeri inseriti sono giusti
    private int controlloNumero(ArrayList<Integer> numInte, ArrayList<Integer> al){
        int num = 0;
        for (int i = 0; i < al.size(); i++) {
            Integer inte = al.get(i);
            if (numInte.contains(inte))
                num++;
        }
        return num;
    }




}